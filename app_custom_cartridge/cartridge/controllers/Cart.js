
var server = require('server');
var Transaction = require('dw/system/Transaction');
server.extend(module.superModule);
server.append('AddProduct', function (req, res, next) {

    if (customer.authenticated) {
        var prof = customer.profile;
        var count = prof.custom.CartCounter == null ? 0 : prof.custom.CartCounter;
        Transaction.wrap(function () {
            prof.getCustom().CartCounter = count + 1;
        });
    }
    next();
});

server.append('Show', function (req, res, next) {
    var currencyService = require('../scripts/init/currencyConversionInit');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');


    var svc = currencyService.getCurrencyConversion();
    var result = svc.call();
    var jsonData = JSON.parse(result.object.text);

    var keys = Object.keys(jsonData.rates);
    var values = [];
    for (var item in jsonData.rates) {
        values.push(jsonData.rates[item]);
    }
    for (var i = 0; i < 9; i++) {
        var n = keys[i];
        Transaction.begin();
        var obj = CustomObjectMgr.getCustomObject('exchangeRates', n);
        if (obj == null) {
            var object = CustomObjectMgr.createCustomObject('exchangeRates', n);
            object.getCustom().currency = n;
            object.getCustom().exchangeValue = values[i];
        }
    }
    Transaction.commit();
    next();
}
);
module.exports = server.exports();
