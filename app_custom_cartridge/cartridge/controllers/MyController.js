'use strict';

var server = require('server');

server.get('Show', function (req, res, next) {
    res.render('/task/page1');
    next();
});


server.post('HandleForm', function (req, res, next) {
    var value1 = parseInt(req.form.mynumber) + 10;

    var OrderMgr = require('dw/order/OrderMgr');
    var order = OrderMgr.getOrder(req.form.ordernum);
    var status = order.getStatus();
    var price = order.getTotalNetPrice();


    var txn = require('dw/system/Transaction');
    txn.wrap(function () {
        order.getCustom().myCounter ? order.getCustom().myCounter += 1 : order.getCustom().myCounter = 1;
    });
    var counter = order.getCustom().myCounter;


    res.render('/task/page2', { value: value1, status: status, price: price, counter: counter });
    next();
});

module.exports = server.exports();
