'use strict';
var server = require('server');

server.get('ModifyProduct', function (req, res, next) {

    var HookMgr = require('dw/system/HookMgr');

    var productID = req.querystring.id;
    var productName = HookMgr.callHook('dw.ocapi.shop.product.modifyGETResponse', 'modifyGETResponse', productID.toString());
    res.json({
        productName: productName
    });

    next();
});

module.exports = server.exports();