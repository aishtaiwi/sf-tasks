'use strict';

var server = require('server');
var Transaction = require('dw/system/Transaction');
var OrderMgr = require('dw/order/OrderMgr');

var checkoutservice = module.superModule;
server.extend(checkoutservice);

server.append('PlaceOrder', function (req, res, next) {

    var viewData = res.getViewData();
    var order =  OrderMgr.getOrder(viewData.orderID);
    Transaction.wrap(function(){
        order.getCustom().productCount = order.getProductQuantityTotal();
    });
    next();
});

    module.exports = server.exports();
