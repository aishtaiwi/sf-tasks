
var server = require('server');
server.extend(module.superModule);


server.append('Show', function (req, res, next) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var ContentMgr = require('dw/content/ContentMgr');


    currentCategory = req.querystring.cgid;

    content = ContentMgr.getContent("category-products");
    var body = content.getCustom().myBody.toString();

    var jsonData = JSON.parse(body);

    var list = [];

    jsonData.forEach(function (obj) {
        productCategorie = ProductMgr.getProduct(obj.ID).getPrimaryCategory();
        if (productCategorie != null) {
            var catID = productCategorie.getID();

        }
        if (catID == currentCategory) {
            list.push(obj.ID)
        }
    });

    var viewData = res.getViewData();
    viewData.list = list;
    res.setViewData(viewData);
    
    next();
});
module.exports = server.exports();
