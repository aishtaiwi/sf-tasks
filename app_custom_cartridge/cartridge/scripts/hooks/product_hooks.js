var Site = require('dw/system/Site');
var ProductMgr = require('dw/catalog/ProductMgr');


function modifyGETResponse(Id) {

    var product = ProductMgr.getProduct(Id).getName();
    var siteName = Site.getCurrent().getName();
    var result = product + " " + siteName;
    
    return result;
}
exports.modifyGETResponse = modifyGETResponse;