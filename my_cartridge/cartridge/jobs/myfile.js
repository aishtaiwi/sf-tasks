
'use strict'
var Transaction  = require('dw/system/Transaction');
var ProductMgr  = require('dw/catalog/ProductMgr');
var ContentMgr  = require('dw/content/ContentMgr');
var URLUtils = require('dw/web/URLUtils');

function execute(){
   var product;
   var link;
   var content;
   
   var jsonData = [];

   var productsCollection = ProductMgr.queryAllSiteProducts();
   while(productsCollection.hasNext()){
        product = productsCollection.next();
        id = product.getID();
        link = URLUtils.https('Product-Show', 'pid',id).toString();

        jsonData.push({
          ID: id,
          ProductLink: link
        });      }
        content = ContentMgr.getContent("category-products");

     
       var str= JSON.stringify(jsonData);

        // JSON.parse();
   
    Transaction.wrap(function(){

      content.getCustom().myBody = str;
   });
}

exports.execute = execute;

















// 'use strict'
// var Transaction  = require('dw/system/Transaction');
// var ProductMgr  = require('dw/catalog/ProductMgr');
// var ContentMgr  = require('dw/content/ContentMgr');
// var URLUtils = require('dw/web/URLUtils');

// function execute(){
//    var product;
//    var name;
//    var link;
//    var content;
   
//    var  stringData = '{ "allProducts" : [';
//    var productsCollection = ProductMgr.queryAllSiteProducts();
//    while(productsCollection.hasNext()){
//         product = productsCollection.next();
//         name = product.getID();
//         link = URLUtils.https('Product-Show', 'pid',name).toString();


//         content = ContentMgr.getContent("category-products");

//         stringData +='{ "' + name + '":" '+ link +'"},' 
//       }
//       stringData+=']}"';
     
//       //  var str= JSON.stringify(jsonData);
//         // JSON.parse();
   
//     Transaction.wrap(function(){

//       content.getCustom().myBody = stringData;
//    });
// }

// exports.execute = execute;
